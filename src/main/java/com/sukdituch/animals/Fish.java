/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Fish extends AquarticAnimal{

    public Fish() {
        super("Fish");
    }

    @Override
    public void swim() {
        System.out.println("Fish : swim");
    }

    @Override
    public void eat() {
         System.out.println("Fish : eat");
    }

    @Override
    public void walk() {
         System.out.println("Fish : walk");
    }

    @Override
    public void speak() {
         System.out.println("Fish : speak");
    }

    @Override
    public void sleep() {
         System.out.println("Fish : sleep");
    }
    
}
