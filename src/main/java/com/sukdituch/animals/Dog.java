/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Dog extends LandAnimal {

    public Dog() {
        super("Dog", 4);
        System.out.println("Dog have : " + "4 legs");

    }

    @Override
    public void run() {
        System.out.println("Dog : run");
    }

    @Override
    public void eat() {
        System.out.println("Dog : eat");
    }

    @Override
    public void walk() {
        System.out.println("Dog : walk");
    }

    @Override
    public void speak() {
        System.out.println("Dog : speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : sleep");
    }

}
