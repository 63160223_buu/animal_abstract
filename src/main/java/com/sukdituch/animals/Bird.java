/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Bird extends Poultry{

    public Bird() {
        super("Bird", 2);
    }

    @Override
    public void fly() {
        System.out.println("Bird : fly");
    }

    @Override
    public void eat() {
        System.out.println("Bird : eat");
    }

    @Override
    public void walk() {
        System.out.println("Bird : walk");
    }

    @Override
    public void speak() {
        System.out.println("Bird : speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bird : sleep");
    }
    
}
