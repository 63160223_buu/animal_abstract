/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Bat extends Poultry{

    public Bat() {
        super("Bat", 2);
        System.out.println("Bat have : 2 legs");
    }

    @Override
    public void fly() {
        System.out.println("bat : fly");
    }

    @Override
    public void eat() {
        System.out.println("bat : eat");
    }

    @Override
    public void walk() {
        System.out.println("bat : walk");
    }

    @Override
    public void speak() {
        System.out.println("bat : speak");
    }

    @Override
    public void sleep() {
        System.out.println("bat : sleep");
    }
    
}
