/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class TestAnimals {

    public static void main(String[] args) {
        Human h1 = new Human("Pan");
        h1.eat();
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        System.out.println("h1 is animals ? " + (h1 instanceof Animals));
        System.out.println("h1 is Land animals ? " + (h1 instanceof LandAnimal));
        System.out.println("");

        System.out.println("If a1 = h1");
        Animals a1 = h1;
        System.out.println("a1 is Land animals ? " + (a1 instanceof LandAnimal));
        System.out.println("a1 is Reptile animals ? " + (a1 instanceof Reptile));
        System.out.println("a1 is Aquartic animals ? " + (a1 instanceof AquarticAnimal));
        System.out.println("a1 is Poultry  ? " + (a1 instanceof Poultry));
        System.out.println("a1 is Human ? " + (a1 instanceof Human));
        System.out.println("...............................................................");

        Earthworm e1 = new Earthworm();
        e1.eat();
        e1.walk();
        e1.speak();
        e1.sleep();
        System.out.println("e1 is animals ? " + (e1 instanceof Animals));
        System.out.println("");

        System.out.println("If a2 = e1");
        Animals a2 = e1;
        System.out.println("a2 is animals ? " + (a2 instanceof Animals));
        System.out.println("a2 is Reptile animals ? " + (a2 instanceof Reptile));
        System.out.println("a2 is Aquartic animals ? " + (a2 instanceof AquarticAnimal));
        System.out.println("a2 is Poultry  ? " + (a2 instanceof Poultry));
        System.out.println("a2 is Human ? " + (a2 instanceof Human));
        System.out.println("a2 is Earthworm ? " + (a2 instanceof Earthworm));
        System.out.println("...............................................................");

        Cat c1 = new Cat();
        c1.eat();
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        System.out.println("c1 is animals ? " + (c1 instanceof Animals));
        System.out.println("c1 is Land animals ? " + (c1 instanceof LandAnimal));
        System.out.println("");

        System.out.println("If a3 = c1");
        Animals a3 = c1;
        System.out.println("a3 is Land animals ? " + (a3 instanceof LandAnimal));
        System.out.println("a3 is Reptile animals ? " + (a3 instanceof Reptile));
        System.out.println("a3 is Aquartic animals ? " + (a3 instanceof AquarticAnimal));
        System.out.println("a3 is Poultry  ? " + (a3 instanceof Poultry));
        System.out.println("a3 is Human ? " + (a3 instanceof Human));
        System.out.println("a3 is Earthworm ? " + (a3 instanceof Earthworm));
        System.out.println("a3 is Cat ? " + (a3 instanceof Cat));
        System.out.println("...............................................................");

        Dog d1 = new Dog();
        d1.eat();
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        System.out.println("d1 is animals ? " + (d1 instanceof Animals));
        System.out.println("d1 is Land animals ? " + (d1 instanceof LandAnimal));
        System.out.println("");

        System.out.println("If a4 = d1");
        Animals a4 = d1;
        System.out.println("a4 is Land animals ? " + (a4 instanceof LandAnimal));
        System.out.println("a4 is Reptile animals ? " + (a4 instanceof Reptile));
        System.out.println("a4 is Aquartic animals ? " + (a4 instanceof AquarticAnimal));
        System.out.println("a4 is Poultry  ? " + (a4 instanceof Poultry));
        System.out.println("a4 is Human ? " + (a4 instanceof Human));
        System.out.println("a4 is Earthworm ? " + (a4 instanceof Earthworm));
        System.out.println("a4 is Cat ? " + (a4 instanceof Cat));
        System.out.println("a4 is Dog ? " + (a4 instanceof Dog));
        System.out.println("...............................................................");

        Snake s1 = new Snake();
        s1.eat();
        s1.walk();
        s1.crawl();
        s1.speak();
        s1.sleep();
        System.out.println("s1 is animals ? " + (s1 instanceof Animals));
        System.out.println("s1 is Land animals ? " + (s1 instanceof Reptile));
        System.out.println("");

        System.out.println("If a5 = s1");
        Animals a5 = s1;
        System.out.println("a5 is Land animals ? " + (a5 instanceof LandAnimal));
        System.out.println("a5 is Reptile animals ? " + (a5 instanceof Reptile));
        System.out.println("a5 is Aquartic animals ? " + (a5 instanceof AquarticAnimal));
        System.out.println("a5 is Poultry  ? " + (a5 instanceof Poultry));
        System.out.println("a5 is Human ? " + (a5 instanceof Human));
        System.out.println("a5 is Earthworm ? " + (a5 instanceof Earthworm));
        System.out.println("a5 is Cat ? " + (a5 instanceof Cat));
        System.out.println("a5 is Dog ? " + (a5 instanceof Dog));
        System.out.println("a5 is Snake ? " + (a5 instanceof Snake));
        System.out.println("...............................................................");

        Crocodile cc1 = new Crocodile();
        cc1.eat();
        cc1.walk();
        cc1.crawl();
        cc1.speak();
        cc1.sleep();
        System.out.println("cc1 is animals ? " + (cc1 instanceof Animals));
        System.out.println("cc1 is Land animals ? " + (cc1 instanceof Reptile));
        System.out.println("");

        System.out.println("If a6 = cc1");
        Animals a6 = cc1;
        System.out.println("a6 is Land animals ? " + (a6 instanceof LandAnimal));
        System.out.println("a6 is Reptile animals ? " + (a6 instanceof Reptile));
        System.out.println("a6 is Aquartic animals ? " + (a6 instanceof AquarticAnimal));
        System.out.println("a6 is Poultry  ? " + (a6 instanceof Poultry));
        System.out.println("a6 is Human ? " + (a6 instanceof Human));
        System.out.println("a6 is Earthworm ? " + (a6 instanceof Earthworm));
        System.out.println("a6 is Cat ? " + (a6 instanceof Cat));
        System.out.println("a6 is Dog ? " + (a6 instanceof Dog));
        System.out.println("a6 is Snake ? " + (a6 instanceof Snake));
        System.out.println("a6 is Crocodile ? " + (a6 instanceof Crocodile));
        System.out.println("...............................................................");

        Fish f1 = new Fish();
        f1.eat();
        f1.walk();
        f1.swim();
        f1.speak();
        f1.sleep();
        System.out.println("f1 is animals ? " + (f1 instanceof Animals));
        System.out.println("f1 is Land animals ? " + (f1 instanceof AquarticAnimal));
        System.out.println("");

        System.out.println("If a7 = f1");
        Animals a7 = f1;
        System.out.println("a7 is Land animals ? " + (a7 instanceof LandAnimal));
        System.out.println("a7 is Reptile animals ? " + (a7 instanceof Reptile));
        System.out.println("a7 is Aquartic animals ? " + (a7 instanceof AquarticAnimal));
        System.out.println("a7 is Poultry  ? " + (a7 instanceof Poultry));
        System.out.println("a7 is Human ? " + (a7 instanceof Human));
        System.out.println("a7 is Earthworm ? " + (a7 instanceof Earthworm));
        System.out.println("a7 is Cat ? " + (a7 instanceof Cat));
        System.out.println("a7 is Dog ? " + (a7 instanceof Dog));
        System.out.println("a7 is Snake ? " + (a7 instanceof Snake));
        System.out.println("a7 is Crocodile ? " + (a7 instanceof Crocodile));
        System.out.println("a7 is Fish ? " + (a7 instanceof Fish));
        System.out.println("...............................................................");

        Crab z1 = new Crab();
        z1.eat();
        z1.walk();
        z1.swim();
        z1.speak();
        z1.sleep();
        System.out.println("z1 is animals ? " + (z1 instanceof Animals));
        System.out.println("z1 is Land animals ? " + (z1 instanceof AquarticAnimal));
        System.out.println("");

        System.out.println("If a8 = z1");
        Animals a8 = z1;
        System.out.println("a8 is Land animals ? " + (a8 instanceof LandAnimal));
        System.out.println("a8 is Reptile animals ? " + (a8 instanceof Reptile));
        System.out.println("a8 is Aquartic animals ? " + (a8 instanceof AquarticAnimal));
        System.out.println("a8 is Poultry  ? " + (a8 instanceof Poultry));
        System.out.println("a8 is Human ? " + (a8 instanceof Human));
        System.out.println("a8 is Earthworm ? " + (a8 instanceof Earthworm));
        System.out.println("a8 is Cat ? " + (a8 instanceof Cat));
        System.out.println("a8 is Dog ? " + (a8 instanceof Dog));
        System.out.println("a8 is Snake ? " + (a8 instanceof Snake));
        System.out.println("a8 is Crocodile ? " + (a8 instanceof Crocodile));
        System.out.println("a8 is Fish ? " + (a8 instanceof Fish));
        System.out.println("a8 is Crab ? " + (a8 instanceof Crab));
        System.out.println("...............................................................");

        Bat b1 = new Bat();
        b1.eat();
        b1.walk();
        b1.fly();
        b1.speak();
        b1.sleep();
        System.out.println("b1 is animals ? " + (b1 instanceof Animals));
        System.out.println("b1 is Land animals ? " + (b1 instanceof Poultry));
        System.out.println("");

        System.out.println("If a9 = b1");
        Animals a9 = b1;
        System.out.println("a9 is Land animals ? " + (a9 instanceof LandAnimal));
        System.out.println("a9 is Reptile animals ? " + (a9 instanceof Reptile));
        System.out.println("a9 is Aquartic animals ? " + (a9 instanceof AquarticAnimal));
        System.out.println("a9 is Poultry  ? " + (a9 instanceof Poultry));
        System.out.println("a9 is Human ? " + (a9 instanceof Human));
        System.out.println("a9 is Earthworm ? " + (a9 instanceof Earthworm));
        System.out.println("a9 is Cat ? " + (a9 instanceof Cat));
        System.out.println("a9 is Dog ? " + (a9 instanceof Dog));
        System.out.println("a9 is Snake ? " + (a9 instanceof Snake));
        System.out.println("a9 is Crocodile ? " + (a9 instanceof Crocodile));
        System.out.println("a9 is Fish ? " + (a9 instanceof Fish));
        System.out.println("a9 is Crab ? " + (a9 instanceof Crab));
        System.out.println("a9 is Bat ? " + (a9 instanceof Bat));
        System.out.println("...............................................................");
        
        Bird bb = new Bird();
        bb.eat();
        bb.walk();
        bb.fly();
        bb.speak();
        bb.sleep();
        System.out.println("bb is animals ? " + (bb instanceof Animals));
        System.out.println("bb is Land animals ? " + (bb instanceof Poultry));
        System.out.println("");

        System.out.println("If aa = bb");
        Animals aa = bb;
        System.out.println("aa is Land animals ? " + (aa instanceof LandAnimal));
        System.out.println("aa is Reptile animals ? " + (aa instanceof Reptile));
        System.out.println("aa is Aquartic animals ? " + (aa instanceof AquarticAnimal));
        System.out.println("aa is Poultry  ? " + (aa instanceof Poultry));
        System.out.println("aa is Human ? " + (aa instanceof Human));
        System.out.println("aa is Earthworm ? " + (aa instanceof Earthworm));
        System.out.println("aa is Cat ? " + (aa instanceof Cat));
        System.out.println("aa is Dog ? " + (aa instanceof Dog));
        System.out.println("aa is Snake ? " + (aa instanceof Snake));
        System.out.println("aa is Crocodile ? " + (aa instanceof Crocodile));
        System.out.println("aa is Fish ? " + (aa instanceof Fish));
        System.out.println("aa is Crab ? " + (aa instanceof Crab));
        System.out.println("aa is Bat ? " + (aa instanceof Bat));
         System.out.println("aa is Bird ? " + (aa instanceof Bird));
    }
}//Finish
