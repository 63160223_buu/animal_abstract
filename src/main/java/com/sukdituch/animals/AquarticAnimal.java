/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public abstract class AquarticAnimal extends Animals{
    
    public AquarticAnimal(String name) {
        super(name, 0);
    }
    public abstract void swim();
}
