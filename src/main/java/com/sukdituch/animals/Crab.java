/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Crab extends AquarticAnimal{
    public Crab() {
        super("Crab");
    }

    @Override
    public void swim() {
        System.out.println("Crab : swim");
    }

    @Override
    public void eat() {
        System.out.println("Crab : eat");
    }

    @Override
    public void walk() {
        System.out.println("Crab : walk");
    }

    @Override
    public void speak() {
        System.out.println("Crab : speakm");
    }

    @Override
    public void sleep() {
        System.out.println("Crab : sleep");
    }

}
