/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Snake extends Reptile{

    public Snake() {
        super("Snake", 0);
        System.out.println("Snake have : 0 leg");
    }

    @Override
    public void crawl() {
        System.out.println("Snake : crawl");
    }

    @Override
    public void eat() {
       System.out.println("Snake : eat");
    }

    @Override
    public void walk() {
        System.out.println("Snake : can't walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake : speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake : sleep");
    }


    
}
