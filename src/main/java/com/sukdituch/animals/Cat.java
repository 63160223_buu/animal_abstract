/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Cat extends LandAnimal{

    public Cat() {
        super("Cat :", 4);
        System.out.println("Cat have : "+ "4 legs");
    }

    @Override
    public void run() {
        System.out.println("Cat : run");
    }

    @Override
    public void eat() {
        System.out.println("Cat : eat");
    }

    @Override
    public void walk() {
        System.out.println("Cat : walk");
    }

    @Override
    public void speak() {
        System.out.println("Cat : speak");
    }

    @Override
    public void sleep() {
        System.out.println("Cat : sleep");
    }
    
}
