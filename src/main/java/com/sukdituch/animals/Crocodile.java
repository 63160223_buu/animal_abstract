/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Crocodile extends Reptile{

    public Crocodile() {
        super("Crocodile", 4);
        System.out.println("Crocodile have : 4 legs");
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile : crawl");
    }

    @Override
    public void eat() {
       System.out.println("Crocodile : eat");
    }

    @Override
    public void walk() {
        System.out.println("Crocodile : walk");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile : speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile : sleep");
    }
    
}
