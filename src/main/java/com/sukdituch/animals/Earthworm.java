/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.animals;

/**
 *
 * @author focus
 */
public class Earthworm extends Animals{

    public Earthworm() {
        super("Earthworm", 0);
        System.out.println("Earthworm have : "+ "0 leg");
    }

    
    @Override
    public void eat() {
        System.out.println("Earthworm : eat");
    }

    @Override
    public void walk() {
        System.out.println("Earthworm : Can't walk but it use creep");
    }

    @Override
    public void speak() {
        System.out.println("Earthworm : speak");
    }

    @Override
    public void sleep() {
        System.out.println("Earthworm : sleep");
    }
    
}
